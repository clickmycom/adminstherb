<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'wp_users';


    protected $fillable = [
        'user_login', 'user_pass', 'user_nicename','user_email','display_name','user_registered','user_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'remember_token',
    ];

    public function getAuthPassword(){
        return $this->user_pass;
    }

//    public function getSystemAttribute()
//    {
//        return $this->attributes['system']=='SmartShop'?1:2;
//    }

}
