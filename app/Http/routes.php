<?php


//----------------------ส่วนกลาง-----------------------------------------------------------------
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::resource('auth','Auth\AuthController');
    Route::get('/', 'HomeController@index');

    //---------------SmartShop------------------
    Route::get('smartshop','SmartShop\DashboardController@index');

    //-----------------Upload-------------------
    Route::get('upload','Upload\DashboardController@index');

});

Route::get('lang/{lang}',function($lang){
    $available = ['en','th'];
    Session::put('locale',
        in_array($lang,$available) ? $lang : Config::get('app.locale'),
        in_array($lang,$available) ? $lang : Date::setLocale('th')
    );
    return redirect()->back();
});

//---------------Control System-------------
Route::get('redirect/{status}',function($status){
    if($status == 200){
        return redirect('');
    }else if($status=='cancle') {
        return redirect()->back();
    }else{
        abort(503);
    }
});
//------------------------------------------

//Route::resource('profiles','ProfilesController');
Route::post('profiles', [
    'as' => 'profiles.update.system', 'uses' => 'ProfilesController@ajaxUpdateUserSystem'
]);
//---------------------------------------------------------------------------------------------


//---------------SmartShop------------------
Route::resource('smartshop/product/group','SmartShop\Product\GroupController');
Route::delete('smartshop/product/group/delete','SmartShop\Product\GroupController@destroy');
Route::get('smartshop/product/group/show','SmartShop\Product\GroupController@show');

//-----------------Upload-------------------
Route::resource('upload/projects/group','SmartShop\Product\GroupController');
Route::delete('upload/projects/group/delete','SmartShop\Product\GroupController@destroy');
Route::get('upload/projects/group/show','SmartShop\Product\GroupController@show');