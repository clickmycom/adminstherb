<?php

namespace App\Http\Controllers\SmartShop;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Lang;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');                   //ตรวจมี Login หรือยัง
    }


    public function index(){

        $data['tasks'] = [
            [
                'name' => 'Design New Dashboard',
                'progress' => '87',
                'color' => 'danger'
            ],
            [
                'name' => 'Create Home Page',
                'progress' => '76',
                'color' => 'warning'
            ],
            [
                'name' => 'Some Other Task',
                'progress' => '32',
                'color' => 'success'
            ],
            [
                'name' => 'Start Building Website',
                'progress' => '56',
                'color' => 'info'
            ],
            [
                'name' => 'Develop an Awesome Algorithm',
                'progress' => '10',
                'color' => 'success'
            ]
        ];

        $system =  Auth::user()->system;
        session()->set('main_menu','dashboard');
        $data['page_title']         =   Auth::user()->system;
        $data['page_description']   =   Auth::user()->system=="SmartShop"?Lang::get("$system\dashboard.page_description"):Lang::get("$system\dashboard.page_description");
        $data['class_icon']         =   Auth::user()->system=="SmartShop"?"fa-shopping-bag bg-red":"fa-cloud-upload bg-light-blue";

        return view('SmartShop/Layouts.content',$data);
    }
}
