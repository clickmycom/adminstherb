<?php

namespace App\Http\Controllers\SmartShop\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductGroup;
//use Illuminate\Http\Request;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use App\Http\Requests\ProductGroupRequest;   //เรียกใช้ตัว ProductGroupRequest ที่สร้างขึ้น
use JsValidator;

class GroupController extends Controller
{
 	public function __construct()
    {
        $this->middleware('auth');                   //ตรวจมี Login หรือยัง
    }

    public function index(){
        session()->set('main_menu','group');
        $this->setsession_menu("index");   
        $groups = ProductGroup::orderBy('products_group_id', 'desc')->get();
        return view('smartshop/product/group.index',compact('groups'));
        // ส่งค่าไปแบบ json 
    }


    public function create(){
        $this->setsession_menu("create");

        $rule = new ProductGroupRequest();
        $validator = JsValidator::make($rule->rules());
    	return view('smartshop/product/group.create')->with([
            'validator' => $validator,
        ]);
    }   

    public function store(ProductGroupRequest $request){
        $input = $request->all();
        $input['products_group_name'] = trim($input['products_group_name']);
        $input['products_group_url'] = trim($input['products_group_url']);
        $input['products_group_imgtitle'] = trim($input['products_group_imgtitle']);
        if(ProductGroup::create($input)){
            $request->session()->flash('status', 'บันทึกข้อมูลเรียบร้อยแล้ว');
            return redirect()->back();
        }
    }


    public function edit($id){
        $group = ProductGroup::find($id);
        if(empty($group)){
            abort(404);
        }
        $rule = new ProductGroupRequest();
        $validator = JsValidator::make($rule->rules());
        return view('smartshop/product/group.edit',compact('group'))->with([
            'validator' => $validator,

        ]) ;
    }

    public function update($id,ProductGroupRequest $request){
         $group = ProductGroup::findOrFail($id);
         $group->update($request->all());
         return redirect('product/group');
    }

    public function destroy($id)
    {
        $group = ProductGroup::findOrFail($id);
        echo $group->delete();
    }

    public function show($id){
        $group = ProductGroup::find($id);
        if(empty($group)){
            abort(404);
        }

        return view('smartshop/product/group.show',compact('group'));
    }

    public function test_insert(){
        // ทดสอบเพิ่มข้อมูล
        // $group = new ProductGroup();
        // $group->group_name='TestAdd';
        // $group->group_code="5555";
        // $group->group_description="5555";
        // $group->status=1;
        // $group->group_at= \Carbon\Carbon::now();
        // $group->save();
        // จบ
    }

    public function setsession_menu($this_sub){
        session()->set('sub_menu',$this_sub);
    }

}
