<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ProfilesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxUpdateUserSystem()
    {
        $user =  Auth::user();
        $user->system = Input::get('system');
        $success = $user->save();
        ($success==TRUE?$txt_status=http_response_code(200):$txt_status=http_response_code(500));
        echo json_encode(array("txt_status"=>"$txt_status"));


    }

}
