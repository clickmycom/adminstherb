<?php

namespace App\Http\Controllers;


use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Lang;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->system=="SmartShop"){
            return redirect('smartshop');
        }else{
            return redirect('upload');
        }
    }

}
