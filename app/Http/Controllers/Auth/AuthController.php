<?php

namespace App\Http\Controllers\Auth;

use App\User;
//use Illuminate\Http\Request;
use Request;    //ใส่ use Request ที่อยู่ใน aliases แทน  use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $username = 'user_login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_login'    => 'required|max:255|unique:wp_users,user_login',
            'user_pass'     => 'required|min:6|confirmed',
            'user_email'    => 'required|email|unique:wp_users,user_email',
            'display_name'  => 'required|min:1',
            'user_nicename' => 'required|min:1',


        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'user_login'        => $data['user_login'],
            'user_pass'         => bcrypt($data['user_pass']),
            'user_email'        => $data['user_email'],
            'display_name'      => $data['display_name'],
            'user_nicename'     => $data['user_nicename'],
            'user_registered'   => date("Y-m-d"),
            'user_status'       =>  0,
        ]);
    }

     public function update($id,Request $request){
         $user = User::find($id);
         $user->update($request->all());
    }

}
