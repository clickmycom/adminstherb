<?php

namespace App\Http\Controllers\Upload;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Lang;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');                   //ตรวจมี Login หรือยัง
    }

    public function index(){
        $data['tasks'] = [
            [
                'name' => 'Design New Dashboard',
                'progress' => '87',
                'color' => 'danger'
            ],
            [
                'name' => 'Create Home Page',
                'progress' => '76',
                'color' => 'warning'
            ],
            [
                'name' => 'Some Other Task',
                'progress' => '32',
                'color' => 'success'
            ],
            [
                'name' => 'Start Building Website',
                'progress' => '56',
                'color' => 'info'
            ],
            [
                'name' => 'Develop an Awesome Algorithm',
                'progress' => '10',
                'color' => 'success'
            ]
        ];

//        $db_ext = \DB::connection('mysql_external');
//        $countries = $db_ext->table('users')->get();
//        print_r($countries);
//        exit;

        $system =  Auth::user()->system;
        session()->set('main_menu','dashboard');
        $data['page_title']         =   $system;
        $data['page_description']   =   Lang::get("$system\dashboard.page_description");
        $data['class_icon']         =   "fa-cloud-upload bg-light-blue";

        return view('Upload/Layouts.content',$data);
    }
}
