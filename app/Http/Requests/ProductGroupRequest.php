<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return [
//            'products_group_name' => 'required|unique:pt_products_group,products_group_name',
//        ];

        switch ($this->method()) {
            case 'GET':
            {
                return [
                    'products_group_name'       =>      'required|unique:pt_products_group,products_group_name,'.$this->get('products_group_id').',products_group_id',
                ];
            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'products_group_name'       =>      'required|unique:pt_products_group,products_group_name',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'products_group_name'       =>      'required|unique:pt_products_group,products_group_name,'.$this->get('products_group_id').',products_group_id',
                ];
            }
            default:break;
        }

    }
}
