<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    //protected $table = 'product_group';  ใช้สำหรับแก้ปัญหาที่มันชอบเติม s ท้าย ตารางใน db
	protected 	$table = 'pt_products_group';
	protected 	$primaryKey = 'products_group_id';
	protected  	$fillable = [
					'products_group_name',
					'products_group_url',
					'products_group_imgtitle'
				];
}