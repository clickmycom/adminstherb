<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wp_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_login',60)->unique();
            $table->string('user_pass',64);
            $table->string('user_nicename',50);
            $table->string('user_email',100);
            $table->string('user_url',100);
            $table->date('user_registered');
            $table->string('user_activation_key',60);
            $table->enum('user_status', array('0', '1'));
            $table->string('display_name',250);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wp_users');
    }
}
