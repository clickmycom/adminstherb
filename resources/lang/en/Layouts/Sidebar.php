<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Online'            => 'Online',
    'Dashboard'         => 'Dashboard',
    'Main_Navigation'   => 'Main&nbsp;Navigation',
    'ProductGroup'      => 'Product&nbsp;Group',
    'GroupViews'        => 'View&nbsp;ProductGroup',
    'GroupAdd'          => 'Add&nbsp;ProductGroup',
];
