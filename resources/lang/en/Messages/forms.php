<?php

return [


    'please'                    =>  'กรุณาระบุข้อมูล',
    'description'               =>  'คำอธิบายรายละเอียดเพิ่มเติม',
    'Button_Save'               =>  'บันทึกข้อมูล',
    'Button_Cancle'             =>  'ยกเลิก',
    'Butoon_Edit'		        =>	'แก้ไขข้อมูล',
    'error'                     =>  'เกิดความผิดพลาด',
    'active'                    =>  'เปิดใช้งาน',
    'unactive'                  =>  'ปิดใช้งาน',
    'status'			        =>	'สถานะ',
    'statused'                  =>  'ผลการทำงาน',

    'swal_title_delete'         =>  'Window product Deletion',
    'swal_text_delete_sec1'     =>  'Are you absolutely sure you want to delete',
    'swal_text_delete_sec2'     =>  'This action cannot be undone.',
    'swal_text_delete_sec3'     =>  'This will permanently delete',
    'swal_text_delete_sec4'     =>  'and remove all collections and materials associations.',

    'DataTable_infoEmpty'       =>  'No records available',
    'DataTable_lengthMenu'      =>  'Display _MENU_ records per page',

    'yadcf_reset'               =>  'Reset all column filters',
    'DataTable_info'            =>  'Showing page _PAGE_ of _PAGES_',
    'DataTable_zeroRecords'     =>  'Nothing found - sorry',

];
