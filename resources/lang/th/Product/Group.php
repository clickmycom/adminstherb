<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

   
    'Add_ProductGroup'          =>      'เพิ่มกลุ่มสินค้า',
    'ProductGroup'              =>      'กลุ่มสินค้า',
    'Name_ProductGroup'         =>      'ชื่อกลุ่มสินค้า',
    'Code_ProductGroup'         =>      'รหัสสินค้า',
    'Description'               =>      'คำอธิบายกลุ่มสินค้า',

    //ตารางแสดงข้อมูล

    'ListProductGroups'         =>          'รายการกลุ่มสินค้า',
    'Group_Code'                =>          'รหัสกลุ่มสินค้า',
    'Group_Name'                =>          'ชื่อกลุ่ม',
    'Group_Description'         =>          'คำอธิบาย',
    'ID'                        =>          'เลขที่',
    'Create_At'                 =>          'สร้างเมื่อ',
    'Status'                    =>          'สถานะ',
    'Action'                    =>          'การกระทำ',
    'Edit'                      =>          'แก้ไข',
    'Modify'                    =>          'ปรับปรุงแก้ไข',
    'Delete'                    =>          'ลบ',
    'View'                      =>          'ดูข้อมูล',
    'Group'                     =>          'กลุ่มสินค้า',  
    'products_group_url'        =>          'URL กลุ่มสินค้า',
    'products_group_name'       =>          'ชื่อกลุ่มสินค้า',
    'products_group_imgtitle'   =>          'URL ที่อยู่รูปภาพ',
];
