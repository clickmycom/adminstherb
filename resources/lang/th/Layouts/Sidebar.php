<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'Online'            => 'ออนไลน์',
    'Dashboard'         => 'แดชบอร์ด',
    'Main_Navigation'   => 'เมนูหลัก',
    'This_System'       => 'คุณกำลังอยู่ในระบบ',
    'Status_System'     => 'สถานะของระบบ',
    'Status_Open'       => 'เปิดใช้งานระบบ',
    'Status_Off'        => 'ปิดใช้งานระบบ',
    /*----------------
        Smart Shop
    -----------------*/

    'ProductGroup'      => 'กลุ่มสินค้า',
    'GroupViews'        => 'แสดงกลุ่มสินค้า',
    'GroupAdd'          => 'เพิ่มกลุ่มสินค้า',

    /*----------------
        Upload
    -----------------*/
    'Projects'          =>  'หัวข้อโปรเจค',
    'Project_View'      =>  'แสดงหัวข้อโปรเจค',
    'Project_Add'       =>  'เพิ่มหัวข้อโปรเจค',

];
