<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'                => 'กรุณาตรวจสอบบัญชีผู้ใช้หรือรหัสผ่าน',
    'throttle'              => 'ความพยายามในการเข้าสู่ระบบมากเกินไป. ปรดลองอีกครั้งใน :seconds วินาที.',
    'register'              => 'ลงทะเบียนผู้ใช้งาน',
    'register_form'         => 'ฟอร์มสำหรับลงทะเบียนผู้ใช้งาน',
    'user_login'            => 'บัญชีผู้ใช้งาน',
    'user_pass'             => 'รหัสผ่าน',
    'user_pass_confirmation' => 'ยืนยันรหัสผ่าน',
    'user_nicename'         => 'ชื่อเล่น',
    'user_email'            => 'อีเมลล์',
    'display_name'          => 'ชื่อในระบบ',
    'button_cancleregister' => 'ยกเลิก',
];
