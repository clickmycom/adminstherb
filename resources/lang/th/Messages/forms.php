<?php

return [


    'please'            =>  'กรุณาระบุข้อมูล',
    'description'       =>  'คำอธิบายรายละเอียดเพิ่มเติม',
    'Button_Save'       =>  'บันทึกข้อมูล',
    'Button_Cancle'     =>  'ยกเลิก',
    'Butoon_Edit'		=>	'แก้ไขข้อมูล',
    'error'             =>  'เกิดความผิดพลาด',
    'active'            =>  'เปิดใช้งาน',
    'unactive'          =>  'ปิดใช้งาน',
    'status'			=>	'สถานะ',
    'statused'          =>  'ผลการทำงาน',
    'Button_Delete'     =>  'ลบข้อมูล',

    'swal_title_delete'         =>  'ยืนยันการลบ',
    'swal_text_delete_sec1'     =>  'คุณแน่ใจหรือไม่ต้องการลบ',
    'swal_text_delete_sec2'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_delete_sec3'     =>  'และลบอย่างถาวร',
    'swal_text_delete_sec4'     =>  'การดำเนินการนี้ไม่สามารถกู้คืนได้',
    'swal_text_success'         =>  'สำเร็จ',
    'swal_text_unsuccess'       =>  'ไม่สำเร็จ',
    'swal_text_success_del'     =>  'ถูกลบเรียบร้อยแล้ว',
    'swal_text_unseccess_del'   =>  'ไม่สามารถลบได้!',

    'DataTable_infoEmpty'       =>  'ไม่พบข้อมูล',
    'DataTable_lengthMenu'      =>  'จำนวนแสดง _MENU_ ข้อมูลต่อหน้า',
    'DataTable_search'          =>  'ค้นหา :',
    'DataTable_first'           =>  'หน้าแรก',
    'DataTable_last'            =>  'หน้าสุดท้าย',
    'DataTable_next'            =>  'หน้าถัดไป',
    'DataTable_previous'        =>  'หน้าที่แล้ว',
    'DataTable_info'            =>  'แสดงหน้า _PAGE_ จาก _PAGES_',
    'DataTable_zeroRecords'     =>  'ไม่พบข้อมูลที่ต้องการ',

    'yadcf_reset'               =>  'รีเซ็ตตัวกรอง',

    //SwitcherSystem
    'switch_title'              =>  'คุณต้องการเปลี่ยนระบบ ?',
    'switch_text'               =>  'กดปุ่มเปลี่ยนระบบ เพื่อเข้าสู่ระบบ',
    'switch_confirmButtonText'  =>  'เปลี่ยนระบบ',

];
