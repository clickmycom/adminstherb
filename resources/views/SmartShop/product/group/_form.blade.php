<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!!$validator!!}

@if ($errors->any())
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i>{{Lang::get('Messages\forms.error')}}</h4>
	</div>
@endif

@if (session()->has('status'))
	<script>
		swal({
			title: "<?php echo session()->get('status'); ?>",
			text: "{{Lang::get('Messages\forms.statused')}}",
			timer: 2000,
			type: 'success',
			showConfirmButton: false
		});
	</script>
@endif

<div class="box-body">

	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<div class="form-group <?=$errors->has('products_group_name')?'has-error':''?>">
		<label  class="col-sm-2 control-label required">{{ Lang::get('Product\Group.products_group_name') }}<font color="red">*</font></label>
		<div class="col-sm-10">
			{!! Form::text('products_group_name',(@$group['products_group_name'])==null?old('products_group_name'):@$group['products_group_name'],['class'=>'form-control','placeholder' =>Lang::get('Messages\forms.please'),'required','onfocus'=>'this.select()']) !!}
			@if($errors->has('products_group_name'))
				<span class="help-block">
                    <strong>{{ $errors->first('products_group_name') }} </strong>
                </span>
			@endif
		</div>
	</div>

	<div class="form-group <?=$errors->has('products_group_url')?'has-error':''?>">
		<label  class="col-sm-2 control-label">{{ Lang::get('Product\Group.products_group_url') }}<font color="red">*</font></label>
		<div class="col-sm-10">
			{!! Form::text('products_group_url',(@$group['products_group_url'])==null?old('products_group_url'):@$group['products_group_url'],['class'=>'form-control','placeholder' =>Lang::get('Messages\forms.please'),'required','onfocus'=>'this.select()']) !!}
					<!-- <input type="text" name="ProductGroup_Code" class="form-control"  value="{{ (@$group['products_group_url'])==null?old('products_group_url'):@$group['products_group_url'] }} "  onfocus="this.select()" placeholder="{{ Lang::get('Messages\forms.please') }}" required> -->
			@if ($errors->has('products_group_url'))
				<span class="help-block">
                        <strong>{{ $errors->first('products_group_url') }} </strong>
                </span>
			@endif
		</div>
	</div>

	<div class="form-group <?=$errors->has('products_group_imgtitle')?'has-error':''?>">
		<label  class="col-sm-2 control-label">{{ Lang::get('Product\Group.products_group_imgtitle') }}<font color="red">*</font></label>
		<div class="col-sm-10">
			{!! Form::text('products_group_imgtitle',(@$group['products_group_imgtitle'])==null?old('products_group_imgtitle'):@$group['products_group_imgtitle'],['class'=>'form-control','placeholder' =>Lang::get('Messages\forms.please'),'required','onfocus'=>'this.select()']) !!}
					<!-- <input type="text" name="ProductGroup_Code" class="form-control"  value="{{ (@$group['products_group_imgtitle'])==null?old('products_group_imgtitle'):@$group['products_group_imgtitle'] }} "  onfocus="this.select()" placeholder="{{ Lang::get('Messages\forms.please') }}" required> -->
			@if ($errors->has('products_group_imgtitle'))
				<span class="help-block">
                        <strong>{{ $errors->first('products_group_imgtitle') }} </strong>
                </span>
			@endif
		</div>
	</div>
</div>

<div class="box-footer ">
	<div class="btn-group">
		<button class="btn {{$btn_type}} ladda-button" data-color="green" type="submit">{{$submitButtonText}}</button>
	</div>

	<div class="btn-group">
		<button type="button" class="btn btn-danger " onclick="location.href='{{ url("smartshop/product/group/")}}'" >{{Lang::get('Messages\forms.Button_Cancle')}}</button>
	</div>

</div>

<script src="http://www.jqueryscript.net/demo/Form-Submit-Buttons-with-Built-in-Loading-Indicators-Ladda/dist/spin.min.js"></script>
<script src="http://www.jqueryscript.net/demo/Form-Submit-Buttons-with-Built-in-Loading-Indicators-Ladda/dist/ladda.min.js"></script>
<script>

	// Bind normal buttons
	Ladda.bind( 'section:not(.progress-demo) button', { timeout: 2000 } );

	// Bind progress buttons and simulate loading progress
	Ladda.bind( 'section.progress-demo button', {
		callback: function( instance ) {
			var progress = 0;
			var interval = setInterval( function() {
				progress = Math.min( progress + Math.random() * 0.1, 1 );
				instance.setProgress( progress );

				if( progress === 1 ) {
					instance.stop();
					clearInterval( interval );
				}
			}, 11200 );
		}
	} );

</script>
