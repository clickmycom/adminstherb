@extends('SmartShop/layouts.dashboard')

@section('content_header')

<h1>
    {{ Lang::get('Product\Group.ProductGroup') }}
    <small>{{ $page_description or null }}</small>
</h1>
<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>{{Lang::get('Product\Group.ProductGroup')}}</a></li>
    <li class="active">{{Lang::get('Product\Group.Add_ProductGroup')}}</li>
</ol>

@endsection

@section('content')
                            <div class="panel panel-primary" >
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-fw fa-bell"></i>
                                        {{ Lang::get('Product\Group.Group')."&nbsp;:&nbsp;".$group->products_group_name }}
                                        <label style="float: right;"><span class="time"><i class="fa fa-clock-o"></i> {{$group->created_at->diffForHumans()}}</span></label>
                                    </h3>
                                    
                                </div>
                                <div class="panel-body">
                                    <div class="box-body">
                                        <dl class="dl-horizontal">

                                            <div class="modal-content">
                                                <div class="modal-header" style="background: #eee;">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title"> {{ $group->products_group_name }}</h4>
                                                </div>
                                                <div class="modal-body">

                                                    <section>
                                                        <dt>
                                                            {{ Lang::get('Product\Group.ID') }}
                                                            <i class="text-danger fa fa-list-ol"></i>
                                                        </dt>
                                                        <p>
                                                        <dd>
                                                            <label>&nbsp;:&nbsp;&nbsp;</label>
                                                            {{ $group->products_group_id }}
                                                        </dd>
                                                        </p>
                                                    </section>

                                                    <section>
                                                        <dt>
                                                            {{ Lang::get('Product\Group.products_group_name') }}
                                                            <i class="text-active fa fa-object-group"></i>
                                                        </dt>

                                                        <p>
                                                        <dd>
                                                            <label>&nbsp;:&nbsp;&nbsp;</label>
                                                            {{ $group->products_group_name }}
                                                        </dd>
                                                        </p>

                                                    </section>

                                                    <section >
                                                        <dt>
                                                            {{ Lang::get('Product\Group.products_group_url') }}
                                                            <i class="text-url  fa fa-external-link"></i>
                                                        </dt>

                                                        <p>
                                                        <dd>
                                                            <label>&nbsp;:&nbsp;&nbsp;</label>
                                                            {{ $group->products_group_url }}
                                                        </dd>
                                                        </p>
                                                    </section>

                                                    <section >
                                                        <dt>
                                                            {{ Lang::get('Product\Group.products_group_imgtitle') }}
                                                            <i class=" fa fa-picture-o"></i>
                                                        </dt>
                                                        <p>
                                                        <dd class="thumbnail">

                                                            <div class="timeline-body">
                                                                <img src="http://placehold.it/150x100" alt="..." class="img-thumbnail" width="150px" height="100px">
                                                            </div>

                                                            <i class="text-primary  fa fa-link"></i>&nbsp;:&nbsp;&nbsp;{{ $group->products_group_imgtitle }}

                                                        </dd>
                                                        </p>

                                                    </section>

                                                    <section>
                                                        <dt>
                                                            {{ Lang::get('Product\Group.Create_At') }}
                                                            <i class="text-unactive fa fa-calendar-o"></i>
                                                        </dt>
                                                        <p>
                                                        <dd>
                                                            <label>&nbsp;:&nbsp;&nbsp;</label>
                                                            {{ $group->created_at }}
                                                        </dd>
                                                        </p>
                                                    </section>

                                                    <section>
                                                        <dt>
                                                            {{ Lang::get('Product\Group.Modify') }}
                                                            <i class="text-unactive fa fa-calendar-o"></i>
                                                        </dt>
                                                        <p>
                                                        <dd>
                                                            <label>&nbsp;:&nbsp;&nbsp;</label>
                                                            {{ $group->updated_at }}
                                                        </dd>
                                                        </p>
                                                    </section>
                                                </div>



                                                <div class="modal-footer" style="background: #eee;">

                                                    <button type="button" class="btn btn-info  pull-left " onclick="location.href='{{ url("smartshop/product/group/")}}'" >
                                                        <i class=" fa fa-ban"></i> {{Lang::get('Messages\forms.Button_Cancle')}}
                                                    </button>

                                                    <button class="btn btn-warning" onclick="location.href='{{ url("smartshop/product/group/$group->products_group_id/edit") }}';" >
                                                        <i class=" fa fa-pencil"></i>&nbsp;&nbsp;{{Lang::get('Messages\forms.Butoon_Edit')}}
                                                    </button>

                                                </div>
                                            </div>

                                        </dl>
                                    </div>
                                    <!-- /.box-body --> </div>
                            </div>


@endsection