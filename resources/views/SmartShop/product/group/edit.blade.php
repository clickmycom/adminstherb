@extends('SmartShop/layouts.dashboard')

@section('content_header')
<h1>
    {{ Lang::get('Product\Group.ProductGroup') }}
    <small>{{ $page_description or null }}</small>
</h1>
<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>{{Lang::get('Product\Group.ProductGroup')}}</a></li>
    <li class="active">{{Lang::get('Product\Group.Add_ProductGroup')}}</li>
</ol>

@endsection

@section('content')
<!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{ Lang::get('Product\Group.Add_ProductGroup') }}</h3>
    </div>

    {!! Form::model($group, ['method' => 'PATCH', 'action' => ['SmartShop\Product\GroupController@update', $group->products_group_id],'class' => 'form-horizontal','id'=>'myform']) !!}
             {!! Form::hidden('products_group_id', $group->products_group_id) !!}
             @include('smartshop\product\group._form',['submitButtonText' => Lang::get('Messages\forms.Butoon_Edit'),'btn_type'=>'btn-warning'])
    {!! Form::close() !!}

  </div>

@endsection