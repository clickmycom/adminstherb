@extends('SmartShop/layouts.dashboard')

@section('content_header')

<h1><i class="fa fa-database"></i>
    {{ Lang::get('Product\Group.ProductGroup') }}
</h1>
<!-- You can dynamically generate breadcrumbs here -->
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>{{Lang::get('Product\Group.ProductGroup')}}</a></li>
    <li class="active">{{Lang::get('Product\Group.Add_ProductGroup')}}</li>
</ol>

@endsection

@section('content')

<!--ส่วนของการลบข้อมูล--!>>
@include('ControlSystem.Form')
@yield('Delete')

    <!-- DataTables -->
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><i class="fa fa-table"></i> {{ Lang::get('Product\Group.ListProductGroups') }}</h3>

            <label class=" control-label" style="float: right;">
                        <span class="time">
                            <button class="btn btn-success" onclick="location.href='{{ url('smartshop/product/group/create') }}';" >
                                <i class=" fa fa-plus-circle"></i>&nbsp;&nbsp;{{Lang::get('Layouts\sidebar.GroupAdd')}}
                            </button>

                            <button class="btn btn-primary" id="clearFilter" onclick="yadcf.exResetAllFilters(tanawat);" >
                                <i class=" fa fa-refresh"></i>&nbsp;&nbsp;{{Lang::get('Messages\forms.yadcf_reset')}}
                            </button>
                        </span>
            </label>

        </div><!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-hover wat">
                <thead style="background: rgba(153, 153, 153, 0.54);">
                <tr>
                    <th style="width: 85px;" ></th>
                    <th style="width: 500px;"></th>
                    <th style="width: 280px;"></th>
                    <th style="width: 180px;"></th>
                    <th class="no-sort" style="width: 150px;">{{ Lang::get('Product\Group.Action') }}</th>
                </tr>
                </thead>
                <tbody>

                @foreach($groups as $group)
                    <tr>
                        <td>{{ $group->products_group_id }}</td>
                        <td>{{ str_limit(($group->products_group_name),30) }}</td>
                        <td>{{ str_limit($group->products_group_url,110) }}</td>

                        <td>
                            {{
                                $group->updated_at->diffForHumans()
                            }}
                        </td>
                        <td align="center"  >
                            |
                            <button class="btn btn-xs btn-default btn-flat" onclick="location.href='{{ url("smartshop/product/group/$group->products_group_id/edit") }}';" >
                                <i class="text-warning fa fa-pencil"></i>
                            </button>
                            |
                            <button type="button" title="{{Lang::get('Product\Group.Delete')}}" class="deleteproductModal btn btn-xs btn-default btn-flat"
                                    data-toggle="modal"
                                    data-destroy_token="{{ csrf_token() }}"
                                    data-destroy_name="{{ $group->products_group_name }}"
                                    data-destroy_route="{{ url('smartshop/product/group/'.$group->products_group_id) }}"
                                    data-destroy_redirect = "{{ url('smartshop/product/group') }}"
                            >
                                <i class="text-danger fa fa-trash"></i>
                            </button>
                            |
                            <button class="btn btn-xs btn-default btn-flat" onclick="location.href='{{ url("smartshop/product/group/$group->products_group_id") }}';" >
                                <i class="fa fa-fw fa-eye text-primary"></i>
                            </button>
                            |
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr style="background-color: #eee;">
                    <th >{{ Lang::get('Product\Group.ID') }}</th>
                    <th >{{ Lang::get('Product\Group.products_group_name') }}</th>
                    <th >{{ Lang::get('Product\Group.products_group_url') }}</th>
                    <th >{{ Lang::get('Product\Group.Create_At') }}</th>
                    <td ></td>
                </tr>

                </tfoot>

            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->


@endsection

@section('script_datatable')
    @include('ControlSystem.DataTable')
@endsection