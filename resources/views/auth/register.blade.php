<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset("auth/images/favicon.ico") }}">

    <!-- App title -->
    <title>Adminto - Responsive Admin Dashboard Template</title>

    <!-- App CSS -->
    <link href="{{ asset("auth/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/core.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/components.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/icons.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/pages.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/menu.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/responsive.css") }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page-register">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Admin<span>to</span></span></a>
        <h5 class="text-muted m-t-0 font-600">Responsive Admin Dashboard</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0"> <i class="fa fa-btn fa-book" style="color:red;"></i>&nbsp;{{ Lang::get('auth.register')}}</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {!! csrf_field() !!}

                            <fieldset>
                                <!-- Textbox (UserPassword) -->
                                <legend>{{ Lang::get('auth.register_form')}}</legend>

                                <div class="form-group{{ $errors->has('user_login') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">{{ Lang::get('auth.user_login')}}</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="user_login" value="{{ old('user_login') }}">

                                        @if ($errors->has('user_login'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('user_login') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Textbox (Password && CC_Password) -->
                                <div class="form-group">
                                    <div class="{{ $errors->has('user_pass') ? ' has-error' : '' }}">
                                        <label class="col-md-3 control-label">{{ Lang::get('auth.user_pass') }}</label>
                                        <div class="col-md-3">
                                            <input type="password" class="form-control" name="user_pass">

                                            @if ($errors->has('user_pass'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('user_pass') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="{{ $errors->has('user_pass_confirmation') ? ' has-error' : '' }}">
                                        <label class="col-md-3 control-label">{{ Lang::get('auth.user_pass_confirmation') }}</label>
                                        <div class="col-md-3">
                                            <input type="password" class="form-control" name="user_pass_confirmation">

                                            @if ($errors->has('user_pass_confirmation'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('user_pass_confirmation') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <!-- Textbox (user_nicename && user_email) -->
                                <div class="form-group">
                                    <div class="{{ $errors->has('user_nicename') ? ' has-error' : '' }}">
                                        <label class="col-md-3 control-label">{{ Lang::get('auth.user_nicename') }}</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" name="user_nicename">

                                            @if ($errors->has('user_nicename'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('user_nicename') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="{{ $errors->has('user_email') ? ' has-error' : '' }}">
                                        <label class="col-md-3 control-label">{{ Lang::get('auth.user_email') }}</label>
                                        <div class="col-md-3">
                                            <input type="email" class="form-control" name="user_email">

                                            @if ($errors->has('user_email'))
                                                <span class="help-block">
                                            <strong>{{ $errors->first('user_email') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <!-- Textbox (display_name) -->
                                <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">{{ Lang::get('auth.display_name') }}</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="display_name" value="{{ old('display_name') }}">

                                        @if ($errors->has('display_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <!-- Button (Submit) -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="button1id"></label>
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-btn fa-user"></i>&nbsp;{{ Lang::get('auth.register')}}
                                        </button>
                                        <button type='button' onclick="window.location.href='{{ url('/login') }}'" id="button2id" name="button2id" class="btn btn-danger">{{ Lang::get('auth.button_cancleregister')}}</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>



        </div>
    </div>
    <!-- end card-box -->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Already have account?<a href="{{ url('/login') }}" class="text-primary m-l-5"><b>Sign In</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->




<!-- jQuery  -->
<script src="{{ asset("auth/js/jquery.min.js") }}"></script>
<script src="{{ asset("auth/js/bootstrap.min.js") }}"></script>

</body>
</html>