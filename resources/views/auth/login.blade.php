
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset("auth/images/favicon.ico") }}">

    <!-- App title -->
    <title>Adminto - Responsive Admin Dashboard Template</title>

    <!-- App CSS -->
    <link href="{{ asset("auth/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/core.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/components.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/icons.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/pages.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/menu.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset("auth/css/responsive.css") }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Admin<span>to</span></span></a>
        <h5 class="text-muted m-t-0 font-600">Responsive Admin Dashboard</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
        </div>
        <div class="panel-body">
            @if ($errors->has())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            <form class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('user_login') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">{{Lang::get('auth.user_login')}}</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="user_login" value="{{ old('user_login') }}">
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">Password</label>
                    <div class="col-md-8">
                        <input type="password" class="form-control" name="password">
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="{{ url('/password/reset') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- end card-box-->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Don't have an account? <a href="{{ url('/register') }}" class="text-primary m-l-5"><b>Sign Up</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->

<!-- jQuery  -->
<script src="{{ asset("auth/js/jquery.min.js") }}"></script>
<script src="{{ asset("auth/js/bootstrap.min.js") }}"></script>

</body>
</html>