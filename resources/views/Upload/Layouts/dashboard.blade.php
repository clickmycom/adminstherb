<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  {{--<meta name="csrf_token" content="{{ csrf_token()}}" />--}}
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{ asset("bootstrap/css/bootstrap.min.css") }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset("dist/css/ionicons.min.css") }} ">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("dist/css/AdminLTE.min.css") }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset("dist/css/skins/_all-skins.min.css") }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("plugins/iCheck/flat/blue.css") }}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ asset("plugins/morris/morris.css") }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset("plugins/jvectormap/jquery-jvectormap-1.2.2.css") }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset("plugins/datepicker/datepicker3.css") }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset("plugins/daterangepicker/daterangepicker-bs3.css") }}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ asset("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") }}">
  <!-- make css -->
  <link rel="stylesheet" href="{{ asset("dist/css/clickmycom.css") }}">
  <!-- Sweet Alert -->
  <script src="{{ asset("plugins/sweetalert/sweetalert.min.js") }}"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset("plugins/sweetalert/sweetalert.css")}}">

  <!-- jQuery 2.1.3 -->
    <script src="{{ asset ("plugins/jQuery/jQuery-2.2.0.min.js") }}" type="text/javascript"></script>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"></div>
</div>

<div class="wrapper ">
      <!-- Header -->
      @include('Upload/layouts.header')
      <!-- Sidebar -->

      @include('Upload/Layouts.sidebar')
      @include('Upload/layouts.config_page')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @yield('content_header')
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Your Page Content Here -->
          @yield('content')
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <!-- Footer -->
      @include('Upload/layouts.footer')
</div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset ("bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset ("dist/js/app.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("dist/js/demo.js") }}" type="text/javascript"></script>

    <!-- Preloader -->
    <script type="text/javascript">
        $(window).load(function() {
            $('#status').fadeOut();
            $('#preloader').delay(5).fadeOut(400);
            $('body').delay(5).css({'overflow':'visible'});
        })
    </script>

    @yield('script_datatable')

</body>
</html>
