

<!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark" >
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li>
        <a href="#control-sidebar-home-tab" data-toggle="tab">
          <i class="fa fa-home"></i>
        </a>
      </li>

    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <div class="tab-pane active" id="control-sidebar-home-tab">
          <h3 class="control-sidebar-heading">{{ Lang::get('Layouts\Sidebar.This_System') }}</h3>

          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:void(0)">
                <i id="icon_system" class="menu-icon fa fa-shopping-bag bg-red"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">
                    <input type="checkbox" id="toggle_two" >

                    <div id="console-event">ระบบ: Smart Shop</div>
                      @include('ControlSystem.SwitcherSystem')
                  </h4>
                  <p>กำลังเปิดใช้งาน</p>
                </div>
              </a>
            </li>
          </ul>

          <?php
          $open_system  = Lang::get('Layouts\Sidebar.Status_Open');
          $off_system   = Lang::get('Layouts\Sidebar.Status_Off');
          ?>

          <h2 class="control-sidebar-heading">{{ Lang::get('Layouts\Sidebar.Status_System') }}</h2>
          <ul class="control-sidebar-menu">

            <li style="background-color:<?=Auth::user()->system=='SmartShop'?'#23527c;':''?>">
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-shopping-bag bg-red"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Smart Shop</h4>
                  <p><i class="fa fa-circle " style="color:<?=Auth::user()->system=='SmartShop'?'#00a65a;':'#dd4b39;'?>"></i>&nbsp;<?=Auth::user()->system=='SmartShop'?$open_system:$off_system?></p>
                </div>
              </a>
            </li>

            <li style="background-color:<?=Auth::user()->system=='Upload'?'#23527c;':''?> ">
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-cloud-upload bg-light-blue"></i>
                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Upload</h4>
                  <p><i class="fa fa-circle " style="color:<?=Auth::user()->system=='Upload'?'#00a65a;':'#dd4b39;'?>"></i>&nbsp;<?=Auth::user()->system=='Upload'?$open_system:$off_system?></p>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

        </div>
      </div>

    </div>
  </aside>
  <!-- /.control-sidebar -->
