<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ asset("dist/img/avatar.png") }}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>{{ Auth::user()->display_name }}</p>
        <!-- Status -->

        <a href="#"><i class="fa fa-circle text-success"></i>{{ Lang::get('Layouts\sidebar.Online') }} </a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->


    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header" >{{ Lang::get('Layouts\sidebar.Main_Navigation') }}</li>
      <li class=" treeview">
        <a href="{{url('upload_system')}}">
          <i class="fa fa-dashboard text-primary"></i> <span>{{ Lang::get('Layouts\sidebar.Dashboard')}}</span>
        </a>
      </li>

      <li class="treeview {{  (session()->get('main_menu')=='group'?'active':'') }}">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>{{ Lang::get('Layouts\sidebar.Projects')}}</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="{{ (session()->get('sub_menu')=='index'?'active':'') }}"><a href="{{ url('product/group') }}"><i class="fa fa-circle-o"></i>{{ Lang::get('Layouts\sidebar.Project_View')}}</a></li>
          <li class="{{ (session()->get('sub_menu')=='create'?'active':'') }}"><a href="{{ url('product/group/create') }}"><i class="fa fa-circle-o"></i> {{ Lang::get('Layouts\sidebar.Project_Add')}}</a></li>
        </ul>
      </li>
    </ul>
    <!-- /.sidebar-menu -->




  </section>
  <!-- /.sidebar -->
</aside>

