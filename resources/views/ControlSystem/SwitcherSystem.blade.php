<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
        $(function() {
            <?php
              if(Auth::user()->system== "SmartShop"){
            ?>
                  $('#toggle_two').attr('checked', 'checked');
                    $('#console-event').html('ระบบ: Smart Shop');
            <?php
              }else{
            ?>
                  $('#toggle_two').removeAttr('checked');
                  $('#console-event').html('ระบบ: Upload');
            <?php
              }
            ?>

            $('#toggle_two').bootstrapToggle({
                on: 'Smart Shop',
                off: 'Upload',
                size: 'normal',
                width: '100%',
                onstyle: 'danger',
                offstyle: 'info',
            });

            var url = "{{route('profiles.update.system')}}";
            $('#toggle_two').change(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                if($(this).prop('checked')){
                    swal( {
                                title: "{{Lang::get("Messages/forms.switch_title")}}",
                                text: "{{Lang::get("Messages/forms.switch_text")}} SmartShop",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "{{Lang::get("Messages/forms.switch_confirmButtonText")}}",
                                cancelButtonText: "{{ Lang::get('Messages\forms.Button_Cancle') }} ",
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if(isConfirm) {
                                    $.ajax({
                                        type: 'POST',
                                        url: url,
                                        data: {system: 1},
                                        dataType: "json",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success: function (data) {
                                            //console.log(data.txt_status);
                                            $('#icon_system').removeClass("fa-cloud-upload bg-light-blue").addClass('fa-shopping-bag bg-red');
                                            $('#console-event').html('ระบบ: Smart Shop');
                                            $('#icon_system').removeClass("fa-cloud-upload bg-light-blue").addClass('fa-shopping-bag bg-red');
                                            $("ul.list-unstyled > li > a").attr("data-skin", "skin-red").trigger("click");
                                            url = "{{ url('redirect')}}/" + data.txt_status;
                                            window.location.replace(url);
                                        },
                                        error: function (data) {
                                            url = "{{ url('redirect')}}/" + data.txt_status;
                                            window.location.replace(url);
                                        }
                                    }); //end ajax send
                                }else{
                                    url = "{{ url('redirect')}}/" + "cancle";
                                    window.location.replace(url);
                                }
                            } // end function confirm
                    );  // end function swal

                }else{
                    swal( {
                                title:"{{Lang::get("Messages/forms.switch_title")}}",
                                text: "{{Lang::get("Messages/forms.switch_text")}} Upload",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "{{Lang::get("Messages/forms.switch_confirmButtonText")}}",
                                cancelButtonText: "{{ Lang::get('Messages\forms.Button_Cancle') }} ",
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if(isConfirm) {
                                    $.ajax({
                                        type: 'POST',
                                        url: url,
                                        data: {system: 2},
                                        dataType: "json",
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success: function (data) {
                                            //console.log(data.txt_status);
                                            $('#icon_system').removeClass("fa-shopping-bag bg-red").addClass('fa-cloud-upload bg-light-blue');
                                            $('#console-event').html('ระบบ: Upload');
                                            $('#icon_system').removeClass("fa-shopping-bag bg-red").addClass('fa-cloud-upload bg-light-blue');
                                            $("ul.list-unstyled > li > a").attr("data-skin", "skin-green").trigger("click");
                                            url = "{{ url('redirect')}}/" + data.txt_status;
                                            window.location.replace(url);
                                        },
                                        error: function (data) {
                                            url = "{{ url('redirect')}}/" + data.txt_status;
                                            window.location.replace(url);
                                        }
                                    }); // end ajax send
                                }else{
                                    url = "{{ url('redirect')}}/" + "cancle";
                                    window.location.replace(url);
                                }
                            } // end function  confirm
                    );  // end function swal
                }// end if

            }); // end function change
        })// end jquery

</script>