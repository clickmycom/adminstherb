<!-- CSS Files -->
<link rel="stylesheet"  href="{{ asset("plugins/datatables/dataTables.bootstrap.css") }} "  type="text/css"/>
<link rel="stylesheet"  href="{{ asset("plugins/resources/css/chosen.min.css") }}"          type="text/css" />

<!-- JS Files -->
<script src="{{ asset("plugins/datatables/jquery.dataTables.min.js") }}"></script>
<script src="{{ asset("plugins/datatables/dataTables.bootstrap.min.js") }}"></script>

<script src="{{ asset("plugins/resources/js/chosen.jquery.min.js")}}"></script>
<script src="{{ asset("plugins/resources/js/jquery.dataTables.yadcf.js")}}"></script>

<script>
    $(document).ready(function () {
        tanawat = $('#example2').dataTable({
            "language": {
                "lengthMenu": "{{ Lang::get('Messages\forms.DataTable_lengthMenu') }}",
                "zeroRecords": "{{ Lang::get('Messages\forms.DataTable_zeroRecords') }}",
                "info": "{{ Lang::get('Messages\forms.DataTable_info') }}",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "search": "{{ Lang::get('Messages\forms.DataTable_search') }}",
                "paginate": {
                    "first": "{{ Lang::get('Messages\forms.DataTable_first') }}",
                    "last": "{{ Lang::get('Messages\forms.DataTable_last') }}",
                    "next": "{{ Lang::get('Messages\forms.DataTable_next') }}",
                    "previous": "{{ Lang::get('Messages\forms.DataTable_previous') }}"
                },
            },

            "stateSave": true, //ใช้สำหรับบันทึกค่าตัวกรอง
            "columnDefs": [{
                "targets": 'no-sort',
                "orderable": false,

            }],


        }).yadcf([

            {
                column_number: 0,
                select_type: 'chosen',
                select_type_options: {
                    no_results_text: 'Can\'t find ->',
                    search_contains: true,
                    width: "85px;",
                },
                column_data_type: "html",
                html_data_type: "text",

                filter_default_label: "{{ Lang::get('Product\Group.ID') }}"
            },

            {
                column_number: 1,
                select_type: 'chosen',
                select_type_options: {
                    search_contains: true,
                    width: "100%;",
                },
                column_data_type: "html",
                html_data_type: "text",
                filter_default_label: "{{ Lang::get('Product\Group.products_group_name') }}"
            },
            {
                column_number: 2,
                select_type: 'chosen',
                select_type_options: {
                    search_contains: true,
                    width: "100%;",
                },
                column_data_type: "html",
                html_data_type: "text",
                filter_default_label: "{{ Lang::get('Product\Group.products_group_url') }}"
            },
            {
                column_number: 3,
                select_type: 'chosen',
                select_type_options: {
                    search_contains: true,
                    width: "100%;",
                },
                column_data_type: "html",
                html_data_type: "text",
                filter_default_label: "{{ Lang::get('Product\Group.Create_At') }}"
            },


        ]);

        // Setup - add a text input to each footer cell
        $('#example2 tfoot th').each( function () {
            var title = $(this).text();
            $(this).html(
                    "<div class='input-group' style='width: 100%;'>"+
                    "<input type='text' id='clear_search' class='form-control ' placeholder="+title+">"+
                    "<span class='input-group-btn'>"+
                    "<button  name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i></button>"+
                    "</span>"+
                    "</div>"
            );
        } );
        // DataTable
        var table = $('#example2').DataTable();

        $('#clearFilter').on('click', function(e){
            $('.wat input[type="text"]').val('');
            tanawat.fnClearTable;
            tanawat.fnSort([[0,'desc']]);
        });

        // Apply the search
        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                            .search( this.value )
                            .draw();
                }
            } );

        } );

        $("div.some-area").text(function(index, currentText) {
            return currentText.substr(0, 1);
        });

    });
</script>