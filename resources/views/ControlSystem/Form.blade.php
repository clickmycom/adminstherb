
@section('Delete')
    <script>
        $(document).ready(function () {

            $('button.deleteproductModal').click(function()
            {
                var DestroyToken = $(this).attr("data-destroy_token");
                var DestroyName = $(this).attr("data-destroy_name");
                var DestroyRoute = $(this).attr("data-destroy_route");
                var DestroyRedirect = $(this).attr("data-destroy_redirect");

                DestroyData(DestroyToken, DestroyName, DestroyRoute,DestroyRedirect);
            });

            function DestroyData(DestroyToken, DestroyName, DestroyRoute,DestroyRedirect)
            {
                swal({
                    title: "{{ Lang::get('Messages\forms.swal_title_delete') }}",
                    text: "{{ Lang::get('Messages\forms.swal_text_delete_sec1') }} \"" + DestroyName + "\" {{ Lang::get('Messages\forms.swal_text_delete_sec2') }}" +
                    "{{ Lang::get('Messages\forms.swal_text_delete_sec3') }}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{ Lang::get('Messages\forms.Button_Cancle') }} ",
                    closeOnConfirm: false,
                    confirmButtonText: "{{ Lang::get('Messages\forms.Button_Delete') }} " + DestroyName,
                    confirmButtonColor: "#ec6c62"
                }, function()
                {
                    $.ajax({
                        type: "DELETE",
                        url: DestroyRoute,
                        headers: { 'X-CSRF-TOKEN' : DestroyToken }
                    }).done(function(data)
                    {
                        if(data == 1){
                            swal("{{ Lang::get('Messages\forms.swal_text_success') }}",DestroyName + " {{ Lang::get('Messages\forms.swal_text_success_del') }}", "success");
                        }
                        setTimeout('redirect("'+DestroyRedirect+'")',1000);

                    }).error(function(data)
                    {
                        swal("{{ Lang::get('Messages\forms.swal_text_unsuccess') }}",DestroyName + " {{ Lang::get('Messages\forms.swal_text_unseccess_del') }}", "error");
                    });
                });
            }
        });

        function redirect(data){
            window.location = data;
        }
    </script>
@endsection

